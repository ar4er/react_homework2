import React, { Component } from "react";
import PropTypes from "prop-types";
import classes from "./Button.module.scss";

export default class Button extends Component {
  render() {
    return (
      <button
        className={classes.Button}
        style={{ background: this.props.backgroundColor }}
        onClick={this.props.onClick}
      >
        {this.props.text}
      </button>
    );
  }
}

Button.defaultProps = {
  backgroundColor: "yellow",
};

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
};
