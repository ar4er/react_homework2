const modalSettings = [
  {
    modalId: "addToBasket",
    settings: {
      header: "Додавання товару до кошика",
      closeButton: true,
      text: "Ви намагаєтесь додати товар до кошика. Продовжити?",
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Поїхали!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Ні, я передумав",
        },
      ],
    },
  },
];

export default modalSettings;
